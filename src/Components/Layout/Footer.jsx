import { Stack, Link, Box } from '@mui/material'
import React from 'react'
import '../Responsive.css'

export const Footer = () => {

    const links = {
        textDecoration: "none",
        padding: '0 15px',
        color: "#333",
        cursor: 'pointer'
    }

  return (
    <Stack sx={{
        width: "auto",
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        padding: '35px 25px',
    }} className="footer">
        <Stack>
            <Box>
                &copy; 2022, made with 	&#10084; by <b>Creative Tim</b> for a better web.
            </Box>
            
        </Stack>
        <Stack sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between'
        }} className="links"
        >
            <Link style={links} href="/">Creative Tim</Link>
            <Link style={links} href="/">About Us</Link>
            <Link style={links} href="/">Blog</Link>
            <Link style={links} href="/">License</Link>
        </Stack>
    </Stack>
  )
}
